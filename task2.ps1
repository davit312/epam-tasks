# Convert accounts data based on naming convention

$csv_path = $ARGS[0]

if (!$csv_path -or !(Test-Path $csv_path)){
    Write-Error "File '$csv_path' not found"
    exit -1
}

$table = Import-Csv $csv_path
$email_count = @{}

$table | %{
    $name_arr = $_.name.ToLower().replace('-',' ') -split ' '    

    $new_name = @()
    $name_arr.foreach({
        $new_name += $_.substring(0,1).ToUpper() + $_.substring(1)
    })
    $_.name = $new_name -join ' '

    $email = $name_arr[0].substring(0,1) + $name_arr[1]
    $email_count[$email] += 1
    $_.email = $email
}
$table | %{
    if($email_count[$_.email] -gt 1){
        $_.email += $_.location_id
    }
    $_.email += '@abc.com'
}

$csv_item = Get-Item $csv_path
$output_csv = $csv_item.Fullname.Remove($csv_item.Fullname.LastIndexOf($csv_item.Name))
$output_csv += $csv_item.Basename + "_new.csv"

$table | Export-Csv -path $output_csv -QuoteFields @('title', 'department')
