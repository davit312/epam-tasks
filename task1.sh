#!/bin/bash

# Convert accounts data based on naming convention

if [[ -z $1 ]]; then
    echo "Please specify accounts file"
    exit 2
fi

if [[ ! -f $1 ]]; then
    echo "File '$1' not found"
    exit 1
fi

accounts_file="$(realpath "$1")"

declare -A employees=()
declare -A emails=()

comma_placeholder='#CMM##'
count=0
while read line || [[ -n $line ]]; do
    # Create title
    if [ -z $head ]; then
        # Add quote on title and department column names
        head=$(echo $line | awk -F , 'BEGIN{OFS=",";}{ print $1,$2,$3,"\""$4"\"",$5,"\""$6"\"" }')
        continue
    fi

    # Replace comma in title or department fileds
    while echo $line | grep -q -E '\".*?,.*?\"'; do
        line=$(echo $line | sed -e 's/\(".*\),\(.*"\)/\1'$comma_placeholder'\2/g')
    done

    # Remove quotes
    line=$(echo $line | sed -e 's/\"//g')

    # Parse name and create email user
    name=$(echo $line | awk -F , '{print tolower($3)}')
    name=$(echo $name | sed 's/[-_]/ /g')

    new_name=$(echo $name | awk '{
        for(i=1;i<=NF;i++){
            $i=toupper(substr($i,1,1)) substr($i,2)
        } }1')
    email=$(echo $name | awk '{print substr($1,1,1)$2}')
    ((emails[$email]++))

    # Create line
    new_line=$(echo $line | awk -F ',' -v name="$new_name" -v email="$email" \
        'BEGIN{OFS=",";}{ print $1,$2,name,"\""$4"\"",email,"\""$6"\"" }')

    employees[$count]="$new_line"
    ((count++))
done < $accounts_file 

new_file_content="$head"$'\n'

# Check email duplicate
for ((i=0; i<$count; i++)); do
    line="${employees[$i]}"
 
    email=$(echo $line | awk -F , '{print $5}')
    new_email="$email"

    if [[ ${emails[$email]} -gt 1 ]]; then
        location=$(echo "$line" | awk -F , '{print $2}')    
        new_email+=$location
    fi

    new_email+="@abc.com"
    line=$(echo "$line" | sed -e "s/$email/$new_email/")

    if [[ $line =~ $comma_placeholder ]]; then
        line=$(echo $line | sed -e 's/'$comma_placeholder'/,/g')
    fi

    new_file_content+="$line"$'\n'
done

outfile=$(dirname $accounts_file)/$(basename $accounts_file .csv)_new.csv

echo -n "$new_file_content" > $outfile