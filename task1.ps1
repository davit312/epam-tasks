# This script should checks if ip_address_1 and ip_address_2 belong to the same network or not

param (
    [Parameter(Mandatory)]$ip_address_1,
    [Parameter(Mandatory)]$ip_address_2,
    [Parameter(Mandatory)]$network_mask
)


function Parse-IPv4Address([string]$IPv4Address) {
    $ip = $IPv4Address -as [ipaddress]
    
    # This check enshures that ip written in correct format 
    if (!$ip -or $IPv4Address -notmatch "^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$" ){
        return $null
    }
    return $ip
}

function Validate-SubnetMask([ipaddress]$SubnetMask) {
    # If network mask is valid then all 1's grouped at left side and all 0's at right side
    $octets = $SubnetMask.GetAddressBytes() | %{[Convert]::ToString($_, 2).PadLeft(8, "0")}
    return !($octets -join "").Contains("01")
}

### main ###

$ip1 = Parse-IPv4Address $ip_address_1
if($ip1 -eq $null){
    Write-Error 'Please enter valid value for ip_address_1'
    exit -1
}

$ip2 = Parse-IPv4Address $ip_address_2
if($ip2 -eq $null){
    Write-Error 'Please enter valid value for ip_address_2'
    exit -2
}

$netmask = Parse-IPv4Address $network_mask
if(!$netmask -or !(Validate-SubnetMask $netmask)){
    Write-Error 'Please enter valid value for network_mask'
    exit -3
}

# Clean host part from ip and compare networks
if(($ip1.Address -band $netmask.Address) -eq ($ip2.Address -band $netmask.Address)){
    Write-Host "yes" # belong same network
}
else{
    Write-Host "no" # different networks
}
