#!/bin/bash

# Convert tests output to json format

if [[ -z $1 ]]; then
    echo "Please specify tests result file"
    exit 2
fi

if [[ ! -f $1 ]]; then
    echo "File '$1' not found"
    exit 1
fi

results_file="$(realpath "$1")"


function make_test(){
echo "{
  \"name\": \"$name\",
  \"status\": $status,
  \"duration\": \"$duration\"
}"
}

output_content=""

while read line || [[ -n $line ]]; do
    


    if [[ -z $testName ]]; then # Parse head
        testName=$(echo $line | awk -F '[\\[\\]]' '{print $2}' |
            awk '{gsub(/^ +| +$/,"")} {print $0}')

    output_content+="{
\"testName\": \"$testName\",
\"tests\": [
"

    elif [[ ${line:0:3} == '---' ]]; then  # Detect separator lines
        ((mode++))

    elif [[ mode -eq 1 ]]; then # Parse test; 
        if echo $line | grep -E -q 'not ok'; then 
            status="false"; 
        else 
            status="true"; 
        fi
        name=$(echo "$line" | awk 'match($0, /[0-9]+  (.*?),/, mtch) { print mtch[1]}')
        duration=$(echo "$line" | awk 'match($0, /([0-9]+ms)$/, mtch) { print mtch[1]}')
        output_content+=$(make_test "$name" "$status" "$duration"),$'\n'


    else # Parse footer

        pased=$(echo "$line" | awk 'match($0, /(^[[:digit:]]+)/, mtch) { print mtch[1]}')
        failed=$(echo "$line" | awk 'match($0, /, ([[:digit:]]+) tests failed/, mtch) { print mtch[1]}')
        rating=$(echo "$line" | awk 'match($0, /rated as ([.0-9]+)%/, mtch) { print mtch[1]}')
        total_duration=$(echo "$line" | awk 'match($0, /spent ([0-9]+ms)/, mtch) { print mtch[1]}')

        output_content="${output_content%??}"$'\n'
        output_content+="],
  \"summary\": {
  \"success\": $pased,
  \"failed\": $failed,
  \"rating\": $rating,
  \"duration\": \"$total_duration\"
 }
}"
    fi

done < $results_file

outfile=$(dirname $results_file)/$(basename $results_file .txt).json

echo -n "$output_content" > $outfile
